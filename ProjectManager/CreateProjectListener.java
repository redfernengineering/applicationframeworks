import javax.swing.JOptionPane;
import javax.swing.JFrame;
import java.io.File;

class CreateProjectListener implements java.awt.event.ActionListener
{
	JFrame appWindow;
	
	public CreateProjectListener(JFrame appWindow)
	{
		this.appWindow = appWindow;
	}
	
	@Override
	public void actionPerformed(java.awt.event.ActionEvent action)
	{
		String projectName = JOptionPane.showInputDialog(null, "Project Name", "");
		File projectDirectory = null;
		
		if (projectName != null) { projectDirectory = new File("projects/" + projectName); }
		if (projectDirectory != null && projectDirectory.getName() != "null") { projectDirectory.mkdir(); }
	}
}