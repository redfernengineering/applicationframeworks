import javax.swing.JOptionPane;
import javax.swing.JLabel;
import java.io.File;

class OpenProjectListener implements java.awt.event.ActionListener
{
	JLabel projectPathDisplay;
	
	public OpenProjectListener(JLabel projectPathDisplay)
	{
		this.projectPathDisplay = projectPathDisplay;
	}
	
	@Override
	public void actionPerformed(java.awt.event.ActionEvent action)
	{
		File projectsDirectory = new File("projects");
		File[] projectsArray = projectsDirectory.listFiles();
		File selectedProject = (File)JOptionPane.showInputDialog(null, null, "Open Project", JOptionPane.INFORMATION_MESSAGE, null, projectsArray, projectsArray[0]);
		this.projectPathDisplay.setText(selectedProject.getName());
	}
}