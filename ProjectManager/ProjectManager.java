import java.io.File;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;

public class ProjectManager
{
	static JFrame window;
	static JMenuBar menuBar;
	static JMenu projectsMenu;
	static JMenuItem createProject, openProject, saveProject, deleteProject;
	static JPanel statusPanel;
	static JLabel statusLabel;
	
	public static void main(String[] args)
	{
		verifyProjectsDirectory();
		initializeMainWindow();
		buildMenuBar();
		
		window.setVisible(true);
	}
	
	static void verifyProjectsDirectory()
	{
		File projectsDirectory = null;
		while (projectsDirectory == null) { projectsDirectory = new File("projects"); }
		
		if (!projectsDirectory.exists() || !projectsDirectory.isDirectory()) { projectsDirectory.mkdir(); }
	}
	
	static void initializeMainWindow()
	{
		window = new JFrame("Project Manager");
		window.setBounds(100, 100, 800, 600);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		menuBar = new JMenuBar();
		projectsMenu = new JMenu("Projects");
		
		statusPanel = new JPanel();
		window.add(statusPanel, BorderLayout.NORTH);
		
		statusLabel = new JLabel();
		statusPanel.add(statusLabel);
	}
	
	static void buildMenuBar()
	{
		addMenuItem(projectsMenu, createProject, "Create", new CreateProjectListener(window));
		addMenuItem(projectsMenu, openProject, "Open", new OpenProjectListener(statusLabel));
		addMenuItem(projectsMenu, saveProject, "Save", new SaveProjectListener());
		addMenuItem(projectsMenu, deleteProject, "Delete", new DeleteProjectListener());
		
		menuBar.add(projectsMenu);
		window.setJMenuBar(menuBar);
	}
	
	static void addMenuItem(JMenu menu, JMenuItem item, String itemName, ActionListener listener)
	{
		item = new JMenuItem(itemName);
		item.addActionListener(listener);
		menu.add(item);
	}
}