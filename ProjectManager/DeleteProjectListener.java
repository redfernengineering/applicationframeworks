import javax.swing.JOptionPane;
import javax.swing.JLabel;
import java.io.File;

class DeleteProjectListener implements java.awt.event.ActionListener
{
	public DeleteProjectListener() { }
	
	@Override
	public void actionPerformed(java.awt.event.ActionEvent action)
	{
		File projectsDirectory = new File("projects");
		File[] projectsArray = projectsDirectory.listFiles();
		File selectedProject = (File)JOptionPane.showInputDialog(null, null, "Delete Project", JOptionPane.INFORMATION_MESSAGE, null, projectsArray, projectsArray[0]);
		
		if (selectedProject != null) { selectedProject.delete(); }
	}
}