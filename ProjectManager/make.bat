rmdir /S /Q app

javac ProjectManager.java
jar -cfm ProjectManager.jar Manifest.mf ProjectManager.class CreateProjectListener.class OpenProjectListener.class SaveProjectListener.class DeleteProjectListener.class

mkdir app
move ProjectManager.jar app/ProjectManager.jar
erase ProjectManager.class
erase CreateProjectListener.class
erase OpenProjectListener.class
erase SaveProjectListener.class
erase DeleteProjectListener.class